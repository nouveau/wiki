Note: For hints for building nouveau on Ubuntu, see [[UbuntuTips|UbuntuTips]] 


### Ubuntu 11.10 Oneric

Things are much easier now.  Install the xserver-xorg-video-nouveau package.  It won't uninstall the proprietary nvidia drivers.   

To switch to nouveau, go to System Settings / Additional Drivers.  Click the activated driver, which is probably "NVIDIA accelerated graphics driver (version current)[Recommended]".  Click "Remove".  With all of these proprietary drivers deactivated and the xserver-xorg-video-nouveau package installed, it will automatically use nouveau.  Copy your /etc/X11/xorg.conf to a backup.  Edit /etc/X11/xorg.conf to change the Driver from "nvidia" to "nouveau".  Reboot. 

The minimal /etc/X11/xorg.conf file, which you should try, if you encounter any problems, is: 
[[!format txt """
Section "Device"
Identifier "n"
Driver "nouveau"
EndSection
"""]]
To revert these changes, just do the opposite.  Go to System Settings / Additional Drivers.  Click "NVIDIA accelerated graphics driver (version current)[Recommended]".  Click "activate".  Restore your backup copy of xorg.conf, or edit it to change the Driver from "nouveau" to "nvidia".  Reboot.  You don't need to uninstall xserver-xorg-video-nouveau. 


## Warning for older releases (before Oneric?)

Nvidia's proprietary libglx module is incompatible to the standard libglx module which is needed for free and open-source drivers such as nouveau. 

Therefore, attempting to install the Ubuntu nouveau package will remove the **nvidia-glx-*** package from the system, making your installation of the proprietary nVidia driver unusable. 

You'll have to reinstall it using apt-get in case you want to the proprietary nVidia driver is a requirement for this.  If you are using the Ubuntu packages the xserver-xorg-video-nouveau package will automatically remove the **nvidia-glx-*** package when it is installed and the nvidia-glx packages will automatically remove the nouveau package when it is installed.  You may wish to remove it manually using the restricted manager and then restart. 

If you installed the nVidia driver (as downloaded from nvidia.com) **manually**, this will **break** your nVidia driver install, and it will need to be reinstalled if you wish to use it again. In that case, ensure that you have a working proprietary nVidia driver installation (e.g. NVIDIA-Linux-x86-190.53-pkg1.run) on your machine(s) before you attempt to install nouveau, because for downloading it from nvidia.com, you need a working JavaScript-capable browser, and re-downloading will be hairy if you are not able to start X because of a broken xorg server installation. 


### Packages in Ubuntu archive

From Jaunty onwards there are nouveau packages in the Universe archive.  Just install xserver-xorg-video-nouveau.  These are built for all architectures supported by Ubuntu, but are not neccesarily up to date. For example, the nouveau package for Ubuntu 9.04 is dated from the month of the 9.04 release, April 2009.   More up to date packages are available for x86 and amd64 in the xorg-edgers PPA or upgrade your distribution (ALT-F2 and then "update-manager -d") to Lucid (alpha) which has a recent nouveau driver release set as the default. 


### Packages and Live-ISO images for Ubuntu 9.10 and 10.4 in the xorg-edgers PPA archive

This only has packages for Ubuntu 9.10 Karmic and 10.4 Lucid. 

[[https://edge.launchpad.net/~xorg-edgers/+archive/nouveau|https://edge.launchpad.net/~xorg-edgers/+archive/nouveau]] 

Add the PPA to your apt sources, update the package lists, and install the **xserver-xorg-video-nouveau** package. 
[[!format txt """
sudo add-apt-repository ppa:xorg-edgers/nouveau
sudo aptitude update && sudo aptitude install xserver-xorg-video-nouveau
"""]]
As of the 15th January 2010, this contains nouveau git code from late November/early December 2009, which is rather recent. 
## Building the xorg-edgers PPA packages from source yourself

The xorg-edgers PPA currently provides only packages for x86 and x86-64, although nouveau is known to run on (at least) PPC. 

The PPA also does not provide packages from Ubuntu 9.04 and older because they do not include and xorg-server package oversion 1.7.0 which is required for recent nouveau code. 

To rebuild the xorg-edgers PPA yourself, you can add the deb and deb-src lines from the PPA, then build the source packages. 

To add the xorg-edgers PPA to your apt repositories (also possible if you run older Ubuntu versions like 9.04), run: 


[[!format txt """
sudo echo -e '\ndeb http://ppa.launchpad.net/xorg-edgers/nouveau/ubuntu karmic main \ndeb-src http://ppa.launchpad.net/xorg-edgers/nouveau/ubuntu karmic main \n'>>/etc/apt/sources.list.d/nouveau-xorg-edgers-ppa.list
sudo apt-get update
"""]]
Note: You can ignore a possible key signature warning triggered by the xorg-edgers PPA at the end of apt-get update.   

Then, to get and compile the sources from the PPA, run: 
[[!format txt """
sudo apt-get install fakeroot module-assistant
sudo apt-get build-dep libdrm2 xserver-xorg-video-nouveau
apt-get -b source {libdrm2,xserver-xorg-video-nouveau}
sudo dpkg --install libdrm*
sudo m-a a-i drm-modules
sudo dpkg --install xserver-xorg-video-nouveau*
"""]]

## Xorg.config

You will then need to select the **nouveau** driver in your /etc/X11/xorg.conf file.  Enter it manually like this: 
[[!format txt """
Section "Device"
    ...
    driver "nouveau"
EndSection
"""]]
Note that Karmic (9.10) does not have an /etc/X11/xorg.conf file by default.  It is created as follows: 
[[!format txt """
sudo Xorg -configure
"""]]
That creates a file, xorg.conf.new in ~/ (the home dir of the user).  Check the file to make sure it contains 'driver "nouveau"' as above.  If not, edit it to add the line.  Then copy to the right location and name: 
[[!format txt """
sudo cp $HOME/xorg.conf.new /etc/X11/xorg.conf
"""]]
Reboot.  If it is still not using the driver, use: 
[[!format txt """
sudo dpkg-reconfigure -phigh xserver-xorg
"""]]
and reboot. 


## Uninstalling

Reverting back to the proprietary nVidia drivers should be as simple as reinstalling the appropriate **nvidia-glx{,-new,-legacy}** package, reverting the xorg.conf change and uninstalling the nouveau packages e.g. (found using dpkg -l|grep nouveau) using dpkg -P. This will leave a git snapshot package of **libdrm2** on your system, but that shouldn't cause problems.  To revert completely, remove the PPA from your apt sources and downgrade the **libdrm2** (and possibly **libdrm-dev**) package. 
