## The irregular Nouveau Development Companion


## Issue #5 for October, 10th


### Introduction

Well, it had to happen. After a hiatus of 4 weeks, the TiNDC is back. Hopefully, there won't be any pauses in the future as long as this one. This companion not only covers the IRC logs but for the first time the DRI mailing list. 

Criticism, ideas and questions can be send in my (KoalaBR) direction on the _#nouveau_ IRC channel, I will try to address them in the next TiNDC. Don't hesitate to post, even if I'm not online, I read the backlogs. 


### Status

Shortly after the release of the last TiNDC we found arlied and marcheu discussing the correct way to  design the driver. The nvidia cards maintains hardware contexts for each program which uses the nvidia card. The first context serves X11, any other OpenGL get another context. The card now  switches between all those contexts via interrrupt. 

As marcheu had some problems getting the card to emit interrupts, arlied and marcheu were talking, whether using only one context would be feasable. Result: doable - yes, but far from efficient, as memory management would get much more complicated in comparison to many different contexts. Besides that we would need to lock the surfaces / memory chunks to certain parts in the logical address space, as user space would react "annoyed" if it's data would move beneath it, without getting any notification. This feature of course would need Kernel based support (kernel module), something which nouveau currently does not need (which makes debugging easy!) 


[[!format txt """
<marcheu> yeah, also, if we do a lock-free driver we have one huge advantage : we can run it under gdb
<marcheu> recall all those times where you'd have killed to gdb radeon_dri.so ?
"""]]
Next problem: the current memory manager by far isn't flexible enough to support something like this. Furthermore,contexts automatically offer some means for memory management and security. As no real conclusion was found,  marcheu decided to post this problem to the DRI list: 

[[http://www.mail-archive.com/dri-devel%40lists.sourceforge.net/msg28472.html|http://www.mail-archive.com/dri-devel@lists.sourceforge.net/msg28472.html]] 

The result was again two options: 

* _For the memory manager, I guess there are two choices:  1) make the  driver use a command-buffer approach even though the hardware supports  per-context ring buffers, or 2) extend the memory manager. _ 
   * _Keith Whitwell_ 
Some other ideas were brought up to (playing MMU tricks), but nothing groundbreaking new came out of it, "only" an evaluation of each possibility, it's advantages and disadvantages. 

At that point, nouveau and its developers faced two major problems: 

* memory management (which needs understanding the object stuff fully) 
* context switching (I think it fails because fifo #2 is actually not correctly setup, some of the magic values should probably be understood reused for that fifo) 
After some more development and testing, it was found, that mode switching (from X11 to terminal or back) was broken. This again lead to a discussion between darktama, arlied and marcheu whether mode switching  should be moved into the kernel. It would have the advantage of sharing code with the nvidia framebuffer but debugging would be more difficult. But as darktama found the problem, this decision was of no  importance and arlied submitted (unrelated to nouveau) a patch to the Linux kernel, which would make management of graphics drivers (DRI,X11 and framebuffer) much easier. So it was decided to ignore that small problem for now. 

A few days after that, marcheu finally got interrupts working. This means, that nouveau now needs to react on the interrupt correctly otherwise the card will stall (or even crash), which is what promptly happened. 

This is the current state of nouveau: Marcheu is searching for the reason of the card's stall and tries to compare it to the binary driver. As we are speaking of interrrupts, this is no easy task and progress is slow (as is the traffic on  #nouveau). 

Darktama has started implementing the state functions for >=NV30 cards, and has been rewriting the shader  code so that most of it is shared between the various implementations (vertex/fragment,NV20/30/40). He also took a brief look at translating Mesa's GLSL bytecode into hw instructions. 


### Help needed

We still are looking for C developers who aren't afraid to get their hands dirty. Writing of the driver could benefit from developers, who already know their way around graphics drivers or aren't afraid to learn. 

[[<<< Previous Issue|NouveauCompanion_4]] | [[Next Issue >>>|NouveauCompanion_6]] 
