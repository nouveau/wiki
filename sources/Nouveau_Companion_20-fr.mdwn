[[!table header="no" class="mointable" data="""
 [[Accueil|FrontPage-fr]]  |  [[TiNDC 2006|IrcChatLogs-fr]]  |  [[TiNDC 2007|IrcChatLogs-fr]]  |  [[Archives anciennes|IrcChatLogs-fr]]  |  [[Archives récentes|IrcChatLogs-fr]]  | DE/[[EN|Nouveau_Companion_20]]/[[ES|Nouveau_Companion_20-es]]/FR/RU/[[Team|Translation_Team]] 
"""]]


## Le compagnon irrégulier du développement de Nouveau (TiNDC)


## Édition du 26 mai 2007


### Introduction

Nous revoilà donc avec le numéro 20 du TiNDC. 

Une page d'introduction permettant d'appréhender la programmation d'un pilote a été écrite par Pq. Elle contient différents liens et documents qui devraient vous donner une vue d'ensemble relativement complète, notamment pour savoir quoi faire quand vous développerez pour Nouveau. 

Notre adhésion au Software Freedom Conservancy (Conservatoire des Logiciels Libres) n'est toujours pas effective. Elle n'a pas été discutée lors de la réunion d'avril mais nous sommes à l'ordre du jour de celle de mai qui devrait se tenir rapidement (nous devrions avoir une réponse d'ici 5 jours au plus). 

Il n'y a toujours pas de changement dans nos problèmes d'infrastructure. Le quota disque de Sourceforge est largement dépassé et aucun autre serveur n'est actuellement disponible. Que cela ne vous empêche pas de continuer à nous envoyer vos dumps, particulièrement ceux que nous ne possédons pas encore, les dumps de configurations SLI ou Quadro étant toujours bienvenus. N'ayez crainte, ils ne seront pas perdus. 


### Statut actuel

Il semble que la branche randr12 est lentement en train de s'améliorer. Killertux a réussi à la faire marcher avec sa NV43, cependant la largeur combinée des deux écrans ne pouvait pas être supérieure à 1280 pixels, ce qui (pour un affichage élargi à 2 écrans) était relativement petit. Puisque Airlied était en vacances, il n'y a pas eu de modification sur la branche. 

Le travail continue sur la série de cartes NV1x. Matc, pmdata et d'autres ont effectué des test plus avancés. Actuellement les cartes se bloquent lorsque certaines commandes de type _viewport_ sont envoyées. Marcheu est presque certain que cela a déjà marché dans le passé, en accord avec ses notes et les commits git autour du 10 mars. Les remarques dans le TiNDC n°15 semblent confirmer cela. Actuellement toutes les cartes jusqu'aux NV3x crashent quand VIEWPORT_ORIGIN est activé, alors qu'il semble aussi que ça fonctionnait, un commit après cette date doit avoir cassé cette fonctionnalité. matc est toujours en train de traquer le bug. 

Le debugging (avec git-bisect) est cependant difficile car aux environs de cette date l'API DRM changeait assez souvent. Toujours est-il que careym s'est porté volontaire pour trouver la cause de l'erreur, mais jusqu'ici sans résultat. 

KoalaBR a finalement ajouté deux autres scripts sur le CVS de sourceforge : 

* crashdump  
Collecte toutes les données intéressantes par rapport à un crash du serveur X11. Ce script est similaire à l'outil nvidia-bug-report.sh. Testez et faites savoir à KoalaBR comment ça marche (ou pas) sur votre système. 
* createdump   
Télécharge, compile, lance et crée une archive (prête à envoyer par mail) des tests Renouveau. Le public visé est uniquement les utilisateurs débutants. Le script explique pas-à-pas et effectue des vérifications. 
Comme promis, maintenant qu'elles sont entre nos mains, l'ingénierie inverse sur les cartes G84 a commencé. Le design des ces cartes semble assez différent des précédentes, cela risque de prendre un peu de temps. 

Pour le moment, nos résultats semblent indiquer : 

* Avec les cartes précédents les NV50, les objets sont référencés par leur offset dans le registre RAMIN. Avec les NV50, chaque canal a sa propre hashtable et chaque objet est référencé relativement à une adresse dépendante du canal. 
* Le PRAMIN PCI BAR est devenu paginé. Il n'est plus simplement projeté dans des blocs de 512Ko à la fin de la VRAM. 
Nous avons déjà ajouté quelques nouveaux objets et commandes à REnouveau. Les dumps devraient donc moins souvent nous renvoyer un "NV50_TCL_PRIMITIVE_3D". 

hellcatv nous a fait savoir que par un pur hasard, il avait assisté à une présentation détaillant un autre projet (déjà bien avancé) d'ingénierie inverse « propre » des cartes G70 et G8x. La personne derrière ce projet n'avait pas entendu parler de Nouveau, hellcatv lui a alors demandé s'il pouvait transmettre son email à notre projet, l'intervenant a semblé intéressé et finalement, Marcheu l'a contacté. Si le résultat de cet échange est fructueux, cela devrait permettre de notablement améliorer notre compréhension des G8x. 

Dans un autre registre, un certain développeur s'est plaint que rules-ng n'est pas assez souvent mentionné dans le TiNDC. Bien que je me permette d'être d'un avis différent, voici quelques informations à ce sujet : 

pq a annoncé qu'il avait commencé à intégrer rules-ng dans mmio-parse. Peu après, Thunderbird, toujours à la recherche d'information sur les registres (dés-)activant la sortie TV des cartes NVIDIA, a demandé plus d'informations. Malheureusement les patchs de pq n'étaient pas encore publiés à ce moment, et Jrmuizel n'avait pas encore eu le temps de les intégrer, ne les ayant reçu que quelques heures auparavant. Après avoir pu tester la nouvelle version, Thunderbird émis quelques question sur le fonctionnement de rules-ng, en effet, ses changements dans la base de données xml n'apparaissaient pas dans mmio-parse. 

La version actuelle de rules-ng nécessite une étape intermédiaire qui parcourt le fichier xml et créé une bibliothèque en C à partir de ce dernier. Seuls les informations compilées dans la base de données apparaitront dans mmio-parse. 

Hum, attendez, on vient juste de me corriger. Il semble évident que j'avais mal compris ce certain développeur, il pensait que ces progrès étaient si infimes qu'ils ne valaient pas la peine d'être rapportés. Et bien, encore une fois, je me permets de ne pas être d'accord. 

Comme c'était la première fois que rules-ng était testé en situation réelle, des bugs ont été trouvés et corrigés, quelques fonctions manquantes ont été ajoutées et ma foi, la vie est belle ! 

Finalement, Thunderbird a réussi à obtenir les données qu'il souhaitait dans les dumps et s'est lancé dans une quête pour obtenir plus d'information sur les registres TV-out des cartes NVIDIA. 

Darktama a commencé à ajouter le support 2D des G8x au pilote. Il a notamment travaillé sur le modesetting (actuellement dans le pilote 2D mais qui devrait aller dans le module DRM) ; mais, pour une raison inconnue, le code échouait à faire ce qu'on attendait de lui. Finalement, après quelques tentatives, darktama a réussi à le faire fonctionner et vous devriez maintenant avoir une 2D fonctionnelle. Attention néanmoins,  il y a de fortes chances que votre affichage soit corrompu lorsque vous revenez du mode texte (NDT : d'un terminal virtuel). Nous travaillons sur ce problème. 

Nous l'avions déjà dit dans une édition précédente, nous ne pouvons pas nous contenter de recopier le code de nv. Il est nécessaire de l'adapter à notre architecture (DRM surtout). Et de surcroît, l'ajout de code obscurci dans Nouveau n'est pas une option envisageable. 


### Demande d'assistance

Nous sommes toujours intéressé par des dumps MMio de cartes ne permettant de faire tourner glxgears correctement. 

Les scripts de KoalaBR mentionnés plus haut ont besoin d'être testés, particulièrement le script crashdump. N'hésitez pas à contacter l'auteur (KoalaBR) pour l'informer de la bonne marche (ou non) des scripts. 

[[<<< Édition précédente|Nouveau_Companion_19-fr]] | [[Édition suivante >>>|Nouveau_Companion_21-fr]] 
